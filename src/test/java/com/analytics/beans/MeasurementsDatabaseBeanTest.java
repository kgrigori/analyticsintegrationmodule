package com.analytics.beans;

import com.analytics.enums.SensorEnum;
import com.analytics.model.Measurement;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Date;

/**
 * Created by kostas.grigoriadis on 05/08/2016.
 */
public class MeasurementsDatabaseBeanTest extends CamelTestSupport {

    private static final Logger LOGGER = Logger.getLogger(MeasurementsDatabaseBeanTest.class);

    @SuppressWarnings("unchecked")
    @Test
    @Ignore
    public void MeasurementPersistTest() throws Exception{

        Measurement measurement = new Measurement("Sensor01", 38, "38", new Date(), SensorEnum.TEMPERATURE.name());
        LOGGER.info("Measurement: " + measurement + " created");

        new MeasurementsDatabaseBean().persistMeasurement(measurement);
    }
}
