package com.analytics.routes;

import com.analytics.enums.SensorEnum;
import com.analytics.utils.Literals;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.impl.JndiRegistry;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Properties;


public class HumidityRouteTest extends CamelTestSupport {


    protected Properties properties;

    private static final String DIRECT_IN = "direct:sensorMeasurementFeed";

    @Ignore
    @Test
    public void testHumidityRouteRoute() throws Exception {

        template.sendBody(DIRECT_IN,
                "SensorTest001:13"
        );
    }


    @Override
    protected void doPostSetup() throws Exception {
        context.getRouteDefinition(Literals.HUMIDITY_ROUTE_FROM)
                .adviceWith(context, new AdviceWithRouteBuilder() {
                    @Override
                    public void configure() throws Exception {
                        // Replacing the file 'from' endpoint
                        replaceFromWith(DIRECT_IN);
                    }
                });
        // we must manually start when we are done with all the advice with
        context.start();
    }

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {

        return new GenericMeasurementRoute(getProperties().getProperty(Literals.FLATPACK_PZMAP_LOCATION), Literals.property(Literals.HUMIDITY_ROUTE_FROM), SensorEnum.HUMIDITY);
    }

    @Override
    protected CamelContext createCamelContext() throws Exception {
        CamelContext camelContext = super.createCamelContext();
        PropertiesComponent propertiesComponent = new PropertiesComponent();
        propertiesComponent.setLocation("classpath:" + Literals.ANALYTICS_INTEGRATION_MODULE_PROPERTIES);

        setProperties(new Properties());
        getProperties().load(GenericMeasurementRoute.class.getClassLoader().getResourceAsStream(
                Literals.ANALYTICS_INTEGRATION_MODULE_PROPERTIES));

        propertiesComponent.setOverrideProperties(getProperties());
        camelContext.addComponent("properties", propertiesComponent);
        return camelContext;
    }

    @Override
    public boolean isUseAdviceWith() {
        // tell we are using advice with, which allows us to advice the route
        // before Camel is being started, and thus can replace activemq with
        // something else.
        return true;
    }

    /**
     * Getters/Setters ~ --------------------------------------------------
     */
    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

}
