package com.analytics.enums;

public enum SensorEnum {
    TEMPERATURE,
    HUMIDITY
}
