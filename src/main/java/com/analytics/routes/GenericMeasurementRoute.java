package com.analytics.routes;

import com.analytics.beans.MeasurementsDatabaseBean;
import com.analytics.enums.SensorEnum;
import com.analytics.processors.MeasurementProcessor;
import com.analytics.utils.FlatpackDataFormatUtil;
import org.apache.camel.builder.RouteBuilder;


public class GenericMeasurementRoute extends RouteBuilder{

    private String flatpackPzmapLocation;
    private String routeFrom;
    private SensorEnum sensorType;


    public GenericMeasurementRoute(String flatpackPzmapLocation, String routeFrom, SensorEnum sensorType) {
        this.flatpackPzmapLocation = flatpackPzmapLocation;
        this.routeFrom = routeFrom;
        this.sensorType = sensorType;
    }


    public void configure() throws Exception {
        from(routeFrom)
            // route id
            .routeId(removeCurlyBraces(routeFrom))
            .autoStartup(true)
            // converts the delimited to map
            .unmarshal(FlatpackDataFormatUtil.configureFlatpackDataFormat(flatpackPzmapLocation, ':'))
            // splits map out of a map (streaming does that!)
            .split().body().streaming()
            // creates Measurement out of a map
            .process(new MeasurementProcessor(sensorType))
            // persist Measurement to database
            .bean(MeasurementsDatabaseBean.class);
    }

    private String removeCurlyBraces(String string){
        return string.replace("{", "").replace("}", "");
    }
}
