package com.analytics.components;

import com.analytics.utils.Literals;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CamelComponents {

    private static final Logger LOGGER = Logger.getLogger(CamelComponents.class);

    private static SessionFactory databaseFactory = null;
    private static final String databaseMutex = "database-mutex";

    /**
     *  database factory
     */
    public static SessionFactory getDatabaseFactory() {
        if (databaseFactory == null) {
            synchronized (databaseMutex) {
                if (databaseFactory == null) {
                    LOGGER.debug("Creating database hibernate session factory");

                    try {
                        databaseFactory = getConfiguration(Literals.HIBERNATE_DATABASE_CFG_XML).
                                buildSessionFactory();
                    } catch (final Exception e) {
                        LOGGER.error("Exception creating DatabaseFactory: ", e);
                    }
                }
            }
        }

        return databaseFactory;
    }

    private static Configuration getConfiguration(final String name) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Configuration configuration = new Configuration();
        configuration.configure(classLoader.getResource(name));
        return configuration;
    }
}
