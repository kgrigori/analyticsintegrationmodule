package com.analytics.beans;

import com.analytics.components.CamelComponents;
import com.analytics.model.Measurement;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class MeasurementsDatabaseBean {

    private final Logger LOGGER = Logger.getLogger(MeasurementsDatabaseBean.class);

    public void persistMeasurement(Measurement measurement){

        LOGGER.info("Processing " + measurement + " Measurement");

        if(measurement != null){
            Session session = CamelComponents.getDatabaseFactory().openSession();
            Transaction transaction = null;

            try {
                transaction = session.beginTransaction();

                session.save(measurement);
                transaction.commit();

                LOGGER.info("Measurement " + measurement + " persisted to the database");
            } catch (Exception e) {
                LOGGER.info("Exception caught while saving measurement: ", e);
                if (transaction != null) {
                    transaction.rollback();
                }
            } finally {
                session.close();
            }

        }else{
            LOGGER.info(measurement + " is null");
        }
    }
}
