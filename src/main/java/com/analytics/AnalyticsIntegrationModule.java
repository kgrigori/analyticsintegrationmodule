package com.analytics;

import com.analytics.enums.SensorEnum;
import com.analytics.routes.GenericMeasurementRoute;
import com.analytics.utils.Literals;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.util.jndi.JndiContext;
import org.apache.log4j.Logger;

import java.util.Properties;


public class AnalyticsIntegrationModule {

    private static final Logger LOGGER = Logger.getLogger(AnalyticsIntegrationModule.class);


    /**
     * Camel objects
     * */
    private CamelContext camelContext;
    private ProducerTemplate producerTemplate;
    protected Properties properties;


    public static void main(String[] args) throws Exception {
        new AnalyticsIntegrationModule().boot();
    }

    public void boot() throws Exception {

        try {
            JndiContext jndiContext = new JndiContext();
            camelContext = new DefaultCamelContext(jndiContext);
            producerTemplate = camelContext.createProducerTemplate();
            /**
             * -------------------------------------------------------------------------------------
             * */

            LOGGER.info("Setting properties file(s) to Camel Context");
            PropertiesComponent propertiesComponent = new PropertiesComponent();
            propertiesComponent.setLocation("classpath:" + Literals.ANALYTICS_INTEGRATION_MODULE_PROPERTIES);

            properties = new Properties();
            properties.load(AnalyticsIntegrationModule.class.getClassLoader().getResourceAsStream(
                    Literals.ANALYTICS_INTEGRATION_MODULE_PROPERTIES
            ));
            propertiesComponent.setOverrideProperties(properties);
            camelContext.addComponent("properties", propertiesComponent);

            // add routes
            LOGGER.info("Adding Routes to Camel Context");
            // Temperature Route
            camelContext.addRoutes(new GenericMeasurementRoute(properties.getProperty(Literals.FLATPACK_PZMAP_LOCATION), Literals.property(Literals.TEMPERATURE_ROUTE_FROM), SensorEnum.TEMPERATURE));
            // Humidity Route
            camelContext.addRoutes(new GenericMeasurementRoute(properties.getProperty(Literals.FLATPACK_PZMAP_LOCATION), Literals.property(Literals.HUMIDITY_ROUTE_FROM), SensorEnum.HUMIDITY));

            LOGGER.info("Starting Camel Context...");
            camelContext.start();

            LOGGER.info("Analytics Integration Module Started.");
        } catch (Exception e) {
            LOGGER.error("Exception caught starting module: ", e);
        }

        Thread.sleep(Long.MAX_VALUE);
    }

}
