package com.analytics.model;

import java.util.Date;


public class Measurement {

    //int measurementId;
    String sensorId;
    int sensorValueNumber;
    String sensorValueTxt;
    Date updateDateTime;
    String sensorType;

    public Measurement(String sensorId, int sensorValueNumber, String sensorValueTxt, Date updateDateTime, String sensorType) {
        this.sensorId = sensorId;
        this.sensorValueNumber = sensorValueNumber;
        this.sensorValueTxt = sensorValueTxt;
        this.updateDateTime = updateDateTime;
        this.sensorType = sensorType;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public int getSensorValueNumber() {
        return sensorValueNumber;
    }

    public void setSensorValueNumber(int sensorValueNumber) {
        this.sensorValueNumber = sensorValueNumber;
    }

    public String getSensorValueTxt() {
        return sensorValueTxt;
    }

    public void setSensorValueTxt(String sensorValueTxt) {
        this.sensorValueTxt = sensorValueTxt;
    }

    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    /*
    public int getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(int measurementId) {
        this.measurementId = measurementId;
    }
    */

    @Override
    public String toString() {
        return "Measurement{" +
                "sensorId='" + sensorId + '\'' +
                ", sensorValueNumber=" + sensorValueNumber +
                ", sensorValueTxt='" + sensorValueTxt + '\'' +
                ", updateDateTime=" + updateDateTime +
                ", sensorType=" + sensorType +
                '}';
    }
}
