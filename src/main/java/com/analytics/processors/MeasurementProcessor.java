package com.analytics.processors;

import com.analytics.enums.SensorEnum;
import com.analytics.model.Measurement;
import com.analytics.utils.StringUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.Map;

public class MeasurementProcessor implements Processor {

    private final Logger LOGGER = Logger.getLogger(MeasurementProcessor.class);
    private SensorEnum sensorType;

    public MeasurementProcessor(SensorEnum sensorType) {
        this.sensorType = sensorType;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Map<String, String> measurementMap = getMeasurementMap(exchange);
        Measurement measurement = convertToMeasurement(measurementMap);

        if(measurement != null){
            exchange.getIn().setBody(measurement);
        }else{
            LOGGER.warn("Route will stop. null measurement detected.");
            exchange.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
        }
    }

    private Map<String, String> getMeasurementMap(Exchange exchange) {
        @SuppressWarnings("unchecked")
        Map<String, String> measurementMap = exchange.getIn().getBody(Map.class);
        if (measurementMap == null) {
            throw new IllegalArgumentException("The exchange body should contain a Map (columnName -> columnValue) representing measurement");
        }
        return measurementMap;
    }

    private Measurement convertToMeasurement(Map<String, String> measurementMap){
        return new Measurement(
                getSensorId(measurementMap),
                getSensorValueNumber(measurementMap),
                String.valueOf(getSensorValueNumber(measurementMap)),
                new Date(),
                getSensorType().name()
                );
    }

    private String getSensorId(Map<String, String> measurementMap){
        return measurementMap.get("sensorId");
    }

    private int getSensorValueNumber(Map<String, String> measurementMap){
        try {
            return StringUtils.toLong(measurementMap.get("sensorValueNumber"));
        } catch (NumberFormatException e) {
            LOGGER.error("Field [sensorValueNumber] cannot be converted to long.");
            return 999;
        }
    }

    public SensorEnum getSensorType() {
        return sensorType;
    }
}
