package com.analytics.utils;

import org.apache.camel.dataformat.flatpack.FlatpackDataFormat;

public class FlatpackDataFormatUtil {
    public static FlatpackDataFormat configureFlatpackDataFormat (final String flatpackPzmapLocation, final char delimiter) {

        FlatpackDataFormat flatpackDataFormat = new FlatpackDataFormat();
        flatpackDataFormat.setIgnoreFirstRecord(false);
        flatpackDataFormat.setDelimiter(delimiter);
        flatpackDataFormat.setDefinition(flatpackPzmapLocation);

        return flatpackDataFormat;
    }
}
