package com.analytics.utils;

public class Literals {
    public static final String HIBERNATE_DATABASE_CFG_XML = "hibernate.cfg.xml";
    public static final String TEMPERATURE_ROUTE_FROM = "temperature.route.from";
    public static final String HUMIDITY_ROUTE_FROM = "humidity.route.from";
    public static final String FLATPACK_PZMAP_LOCATION = "flatpack.pzmap.location";

    public static final String ANALYTICS_INTEGRATION_MODULE_PROPERTIES = "analyticsintegrationmodule.properties";

    /**
     * Enclose variable into curly brackets for Camel routes endpoints
     */
    public static String property(final String variable) {
        String result;

        if (variable != null) {
            result = "{{" + variable + "}}";
        } else {
            result = "Variable is null";
        }

        return result;
    }
}
