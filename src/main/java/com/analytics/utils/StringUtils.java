package com.analytics.utils;


public class StringUtils {

    public static int toLong(String value) {
        return value == null ? 0 : Integer.parseInt(value);
    }
}
