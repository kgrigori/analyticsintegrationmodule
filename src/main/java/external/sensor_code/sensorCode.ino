// Libraries
#include <DHT.h>
#include <DHT_U.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Sensor values
#define DHTPIN   2  // Pin which is connected to the DHT sensor.
#define DHTTYPE  DHT11 // DHT 11
DHT_Unified      dht(DHTPIN, DHTTYPE);
#define delayMS  10*60000 //10 mins

// Wifi values
#define wifi_ssid "SKY31035"
#define wifi_password "CBTRVBBR"

// MQTT values
#define mqtt_server "192.168.0.200"
#define topic_temperature  "topic.temperature"
#define topic_humidity     "topic.humidity"

WiFiClient espClient;
PubSubClient client(espClient);

uint8_t MAC_array[6];
char MAC_char[18]; // global variable for mac address of the sensor
String output;

// lastMsg is used in loop()
long lastMsg = 0;

/************************
 * Additional Functions *
 ***********************/

void setup_sensor() {
    
    // Initialize device.
    dht.begin();
    Serial.println("DHT11 Unified Sensor Initialization");
    // Print temperature sensor details.
    sensor_t        sensor;
    dht.temperature().getSensor(&sensor);
    Serial.println("------------------------------------");
    Serial.println("Temperature");
    Serial.print("Sensor:       ");
    Serial.println(sensor.name);
    Serial.print("Driver Ver:   ");
    Serial.println(sensor.version);
    Serial.print("Unique ID:    ");
    Serial.println(sensor.sensor_id);
    Serial.print("Max Value:    ");
    Serial.print(sensor.max_value);
    Serial.println(" *C");
    Serial.print("Min Value:    ");
    Serial.print(sensor.min_value);
    Serial.println(" *C");
    Serial.print("Resolution:   ");
    Serial.print(sensor.resolution);
    Serial.println(" *C");
    Serial.println("------------------------------------");
    
    // Print humidity sensor details.
    dht.humidity().getSensor(&sensor);
    Serial.println("------------------------------------");
    Serial.println("Humidity");
    Serial.print("Sensor:       ");
    Serial.println(sensor.name);
    Serial.print("Driver Ver:   ");
    Serial.println(sensor.version);
    Serial.print("Unique ID:    ");
    Serial.println(sensor.sensor_id);
    Serial.print("Max Value:    ");
    Serial.print(sensor.max_value);
    Serial.println("%");
    Serial.print("Min Value:    ");
    Serial.print(sensor.min_value);
    Serial.println("%");
    Serial.print("Resolution:   ");
    Serial.print(sensor.resolution);
    Serial.println("%");
    Serial.println("------------------------------------");
}
   
void setup_wifi() {
    delay(10);
    // Start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(wifi_ssid);
    WiFi.begin(wifi_ssid, wifi_password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println("Sensor's MAC address: ");

    // Parsing the mac address of the sensor
    WiFi.macAddress(MAC_array);
    for (int i = 0; i < sizeof(MAC_array); ++i){
      sprintf(MAC_char,"%s%02x",MAC_char,MAC_array[i]);
    }
    Serial.println(MAC_char); 
    
}

void reconnect() {
    // Loop until we're reconnected
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        if (client.connect("TestMQTT")) { //* See //NOTE below
            Serial.println("connected");
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

void pubMQTT(String topic, String topic_val){
    Serial.print("Newest topic " + topic + " value:");
    Serial.println(topic_val);
    client.publish(topic.c_str(), topic_val.c_str(), true);
}

/*************************** Sketch Code ************************************/
void setup(){

    pinMode(LED_BUILTIN, OUTPUT); // Initialize the BUILTIN_LED pin as an output
    Serial.begin(115200);
  
    setup_wifi();
    client.setServer(mqtt_server, 1883);
    
    delay(5000);
    setup_sensor();
}

void loop(){

    if (!client.connected()) {
      reconnect();
    }
    client.loop();
    
    //10 seconds minimum between Read Sensors and Publish
    long now = millis();
  
    if (now - lastMsg > 10000) {
        lastMsg = now;
        // Temp variables
        int temperature;
        int humidity;
        
        digitalWrite(LED_BUILTIN, LOW); // Turn the LED on (Note that LOW is the voltage level)
        
        // Get temperature event and print its value.
        sensors_event_t event;
        dht.temperature().getEvent(&event);
      
        Serial.println("DHT11 Sensor Readings--");
        if (isnan(event.temperature)) {
          Serial.println(" Error reading temperature!");
        } else {
          temperature = event.temperature;
          
          Serial.print(" Temperature: ");
          Serial.print(temperature);
          Serial.println(" *C");
        }

        output = String(MAC_char) + ":" + temperature;
        
        //Publish temprature to topic.temperature topic on MQTT broker
        pubMQTT(topic_temperature,output);
        
        // Get humidity event and print its value.
        dht.humidity().getEvent(&event);
        if (isnan(event.relative_humidity)) {
          Serial.println("Error reading humidity!");
        } else {
          humidity = event.relative_humidity;
          
          Serial.print(" Humidity: ");
          Serial.print(humidity);
          Serial.println("%");
        }
      
        output = String(MAC_char) + ":" + humidity;
        
        //Publish humidity to topic.humidity topic on MQTT broker
        pubMQTT(topic_humidity,output);
        
        digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
      
        // Delay between measurements.
        delay(delayMS);
    }
}
